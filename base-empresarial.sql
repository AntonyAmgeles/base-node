-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 02-07-2019 a las 23:41:22
-- Versión del servidor: 10.1.40-MariaDB
-- Versión de PHP: 7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `base-empresarial`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emp_modulo`
--

CREATE TABLE `emp_modulo` (
  `idmodulo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `icono` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `emp_modulo`
--

INSERT INTO `emp_modulo` (`idmodulo`, `nombre`, `icono`) VALUES
(1, 'Dashboard', 'fas fa-tachometer-alt'),
(2, 'Usuarios', 'fas fa-user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emp_perfiles`
--

CREATE TABLE `emp_perfiles` (
  `idperfil` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `descripcion` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `emp_perfiles`
--

INSERT INTO `emp_perfiles` (`idperfil`, `nombre`, `descripcion`) VALUES
(1, 'Administrador', 'administrador de sistemas General'),
(2, 'Soporte', 'Soporte Tecnico de la base en node');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `emp_submodulo`
--

CREATE TABLE `emp_submodulo` (
  `idsubmodulo` int(11) NOT NULL,
  `idmodulo` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `ruta` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `emp_submodulo`
--

INSERT INTO `emp_submodulo` (`idsubmodulo`, `idmodulo`, `nombre`, `ruta`) VALUES
(1, 1, 'Dashboard Admin', 'dash-admin'),
(2, 2, 'Usuarios', 'usuarios'),
(3, 2, 'Perfiles', 'perfiles'),
(4, 1, 'Dashboard Usuario', 'dash-user');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `idusuario` int(11) NOT NULL,
  `nick_name` varchar(50) NOT NULL,
  `password` varchar(255) NOT NULL,
  `estado` tinyint(1) NOT NULL,
  `observacion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`idusuario`, `nick_name`, `password`, `estado`, `observacion`) VALUES
(1, 'admin', '123', 1, 'administrador del sistema');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `emp_modulo`
--
ALTER TABLE `emp_modulo`
  ADD PRIMARY KEY (`idmodulo`);

--
-- Indices de la tabla `emp_perfiles`
--
ALTER TABLE `emp_perfiles`
  ADD PRIMARY KEY (`idperfil`);

--
-- Indices de la tabla `emp_submodulo`
--
ALTER TABLE `emp_submodulo`
  ADD PRIMARY KEY (`idsubmodulo`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`idusuario`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `emp_modulo`
--
ALTER TABLE `emp_modulo`
  MODIFY `idmodulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `emp_perfiles`
--
ALTER TABLE `emp_perfiles`
  MODIFY `idperfil` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `emp_submodulo`
--
ALTER TABLE `emp_submodulo`
  MODIFY `idsubmodulo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `idusuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
