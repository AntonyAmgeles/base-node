/*--===========================================
 CARGANDO LOS PAQUETES
===========================================*/
'use strict'
const express = require('express');
const cookieParser = require('cookie-parser');
const mysql = require('mysql');
const hbs = require('hbs');
const config_db= require('./conexion/conexion');
//const bodyParser = require('body-parser');
const routes = require('./routes/index');
const router = express.Router();
const app = express();

const port = process.env.PORT || 2000;

app.use(cookieParser());
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // <- usa el modo extendido siempre
app.use(express.static(__dirname + '/public'));
// // Express HBS engine
hbs.registerPartials(__dirname + '/views/parciales');
app.set('view engine', 'hbs');
app.set('views', __dirname + '/views');
app.use(routes(router));
app.use(require('./modelos/index'));


app.listen(port, () => {
    console.log(`Escuchando peticiones en el puerto ${ port }`);
});
