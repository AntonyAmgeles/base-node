/*
prueba += `
<li class="slide">
<a class="side-menu__item active" data-toggle="slide" href="#"><i class="side-menu__icon fe fe-home"></i><span class="side-menu__label">Dashboard</span><i class="angle fa fa-angle-right"></i></a>
    <ul class="slide-menu">
        <li>
            <a class="slide-item" href="index-2.html">Retail Sales Dashboard</a>
        </li>
        <li>
            <a class="slide-item" href="dashboard-social.html">Social Dashboard</a>
        </li>
        <li>
            <a class="slide-item" href="dashboard-marketing.html">Marketing Dashboard</a>
        </li>
        <li>
            <a class="slide-item" href="dashboard-it.html">IT Dashboard</a>
        </li>
        <li>
            <a class="slide-item" href="dashboard-cryptocurrency.html">Cryptocurrency Dashboard</a>
        </li>

    </ul>
</li>
`
*/
let dashboard = {
    menu: [],
    submenu : []
};
let menu = '';
$(document).ready(function(){
    
    getDataMenu();
    setTimeout(function(){
        for(let i in dashboard.menu){
        
            menu += `
                <li class="">
                <a class="side-menu__item active" data-toggle="slide" href="#"><i class="side-menu__icon ${dashboard.menu[i]['icono']}"></i><span class="side-menu__label">${dashboard.menu[i]['nombre']}</span><i class="angle fa fa-angle-right"></i></a>
                <ul class="slide-menu">
                `
            
            menu += retornarLi(dashboard.menu[i]['idmodulo']); 
            
            menu +=`</ul></li>`
            
           
        }
        
        $("#nav-menu").append(menu)
        $("#nav-menu > li").addClass("slide");
        /*
        $("#nav-menu li").on("click", function(){
            
        })
        */
       
        
    },100)
    
    
    
})


function retornarLi(idmodulo){
    let li = ``;
    for(let e in dashboard.submenu){
        if(dashboard.submenu[e]['idmodulo'] == idmodulo){
            li += `
                
                    <li>
                        <a class="slide-item" href="${dashboard.submenu[e]['ruta']}">${dashboard.submenu[e]['nombre']}</a>
                    </li>
                
                    `
        }
    }
    return li;
}

function getDataMenu(){
    // =======================MENU=====================================
    fetch(`${window.ruta}/menus`,{
        method:"GET",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
    .then(res=>res.json())
    .then( res => {
        let data = res.modulos;
        
        for(i in data){
            dashboard.menu.push({
                'idmodulo': data[i]['idmodulo'],
                'nombre': data[i]['nombre'],
                'icono' : data[i]['icono']
            })
        }
        
    }).catch(error => console.log(error))

    //================================SUB MENU==============================
    fetch(`${window.ruta}/submenu`,{
        method:"GET",
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
    })
    .then(res => res.json())
    .then(res => {
        let datos = res.submodulos
        
        for(e in datos){
            dashboard.submenu.push({
                'idmodulo': datos[e]['idmodulo'],
                'nombre': datos[e]['nombre'],
                'ruta': datos[e]['ruta']
            })
        }
        
    })
    .catch(error => console.log(error))
}



