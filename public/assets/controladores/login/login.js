$(document).ready(function(){
    $(".btningresar").click(() => {
        let user = $(".nick").val();
        let pass = $(".pass").val();
        let datos = {
            'nick': user,
            'pass': pass
        }

        fetch(`${window.ruta}/login`,{
            method: 'POST',
            body: JSON.stringify(datos),
            headers: {'Content-Type': 'application/json'},
        })
        .then(res => res.json())
        .then(res => {
            console.log(res);
             if(res.ok){
                swal({
                    type: 'success',
                    title: 'Bienvenido!',
                    showConfirmButton: false,
                    timer: 1500
                });
                localStorage.setItem("lastToken",res.token);
                document.cookie=`token=${res.token}`;
                window.location.href = `${window.ruta}/dashboard`;
             }else{
                swal({
                    type: 'error',
                    title: 'Usuario o contraseña Incorrectas!',
                    showConfirmButton: false,
                    timer: 1500
                })
                $(".nick").val('');
                $(".pass").val('');
                document.cookie=`token=;expires=Thu, 01 Jan 1970 00:00:01 GMT;`
             }
        })
        
    })
   
})