/*======================================
CARGANDO FICHEROS DE LOGIN
========================================*/

require("../config/vars-global");
const jwt = require('jsonwebtoken');


/*=====================================
TODO: INICIO VERIFICAR TOKEN
=======================================*/

let verificaToken = (req, res, next) => {

    let token = req.cookies.token;

    jwt.verify(token,process.env.SEED, (err, decoded) => {

        if (err) {

            return res.status(401).json({
                ok:false,
                err
            });
           
        }
        req.nick = decoded.nick;
        
        next();

    })

};


/*===================================
FIXME: FIN VERIFICAR TOKEN
=====================================*/

module.exports={
    verificaToken
}
