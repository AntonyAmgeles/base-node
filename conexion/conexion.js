const bd_config = require('../config/database');
const mysql = require('mysql');

const connectionsql = mysql.createConnection({
  host     : 'localhost',
  user     : 'root',
  password : '',
  database : 'base-empresarial'
});


module.exports = {
  connectionsql
}