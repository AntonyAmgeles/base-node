//RUTAS
const routes = (router) => {
    // Aqui manejas tus rutas, debes tomar en cuenta el orden en que usas las mismas
    // recuerda que las mismas se leen en cascada y en orden, por lo tanto si la primera
    // ruta apunta a '/', las demás no serán accedidas, ya que siempre entrará primero
    // a la ruta raíz, puedes leer más en la documentación
    router.route('/')
        .get((req, res, next) => {
            return res.status(200).render('login/login');
        });


    router.route('/dashboard')
    .get((req,res) =>{
      return res.status(200).render('dashboard/dashboard');
     });

    return router;
}

module.exports = routes;