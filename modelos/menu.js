//uso de express
const express = require('express');
//inicializo express
const app = express();
//uso de la conexion
const {connectionsql} = require('../conexion/conexion');

const { verificaToken } = require('../middleware/authentication');
require('../config/vars-global');

app.get('/menus', verificaToken ,(req, res) => {

    connectionsql.query(`select * from emp_modulo`, (error,results,fields) =>{
      if (error) throw error
        if(results.length <= 0){
          return  res.json({
            ok:false,
            modulos:[],
            rpta:"no existen modulos"
          })
        }

        return res.json({
          ok:true,
          modulos: results
        })
         
    })
    
})
app.get('/submenu', verificaToken ,(req , res) => {
    connectionsql.query(`SELECT idmodulo,nombre,ruta FROM emp_submodulo `, (error , results, fields)=> {
      if (error) throw error
        if(results.length <= 0){
          return  res.json({
            ok:false,
            submodulos:[],
            rpta:"no existen Submodulos"
          })
        }
        return  res.json({
          ok:true,
          submodulos:results,
        })
        
    })
})

app.post('/logout', verificaToken, (req, res) => {

  res.clearCookie('token', { path: '/' });

  res.redirect("/");

});

module.exports = app
