//uso de express
const express = require('express');
//inicializo express
const app = express();
const bcrypt = require('bcrypt');
//uso de la conexion
const {consultaSQL,insercionSQL} = require('../conexion/consultas');


// TODOS LOS USUARIOS
app.get("/consultarusuarios",(req,res)=>{

  consultaSQL(`select * from usuarios `)
  .then(result=>{
      
      if(result.length<=0){

         return  res.json({
              ok:false,
              usuarios:[],
              rpta:"no existe usuarios"
          })
      }

      res.json({
          ok:true,
          usuarios:result
      })
  
  
  })
  .catch(err=> console.log(err))

})

//Buscar USUARIO ESPECIFICO
app.get('/consultausuario:id', (req, res) => {
    const { id } = req.params; 
    consultaSQL(`select * from usuarios where idusuario=${id}`)
  .then(result=>{
      
      if(result.length<=0){

         return  res.json({
              ok:false,
              usuarios:[],
              rpta:"no existe usuarios"
          })
      }

      res.json({
          ok:true,
          usuarios:result
      })
  
  
  })
  .catch(err=> console.log(err))
});

//INSERTAR UN USUARIO
app.post('/insertarusuario', (req, res) => {
    const {nick_name,password,observacion } = req.body;
    consultaSQL(`select * from usuarios where nickname=${nick_name}`)
    .then(result=>{

        if(result.length>0){

            return  res.json({
                ok:false,
                mensaje:"usuario ya existe"
            })
        }

        // INGRESANDO EL NUEVO PROVEEDOR

        insercionSQL(`insert into usuarios(nickname,password,observacion) values('${nickname}','${password}',1,${observacion})`)
        .then(result=>{
            
            res.json({
                ok:true,
                usuarios:result.rowsAffected[0]
            })
        
        
        })
        .catch(err=> console.log(err))


    }).catch(err=> console.log(err))
  
});

//EDITAR UN USUARIO
app.post('/editarusuario', (req, res) => {
    const {id, nick_name,password,observacion } = req.body;
    
    insercionSQL(`UPDATE usuarios SET nickname = '${nick_name}', password = '${password}', observacion'${observacion}' WHERE idusuario = '${id}'`)
    .then(result => {
      res.json({
          ok:true,
          usuarios:result.rowsAffected[0] 
      })
    })
    .catch(err => console.log(err))
});

//ESTADO ACTIVO
app.post('/',(req, res) => {
    const {id} = req.body;
    
    insercionSQL(`UPDATE usuarios SET estado = 1 WHERE idusuario="${id}`)
    .then(result => {
      res.json({
          ok:true,
          usuarios:result.rowsAffected[0] 
      })
    })
    .catch(err => console.log(err))
    
    
});

//ESTADO INACTIVO

app.post('/',(req, res) => {
    const {id} = req.body;
    insercionSQL(`UPDATE usuarios SET estado = 0 WHERE idusuario="${id}`)
    .then(result => {
      res.json({
          ok:true,
          usuarios:result.rowsAffected[0] 
      })
    })
    .catch(err => console.log(err))
    
});
module.exports = app